
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installing
 * Configuration
 * Known Issues

INTRODUCTION
------------

The Jobvite Careers module lets you integrate your company's Jobvite account
into Drupal. 

The module grabs the XML file provided by Jobvite to generate a careers page
with a categorized careers listing, which links to an individual Drupal page 
for each position.


INSTALLING
----------

Install the module like all normal modules, by enabling it at admin/modules.
The module will be visible in the "other" category.


CONFIGURATION
-------------

Once you have installed and enabled the module, the configuration can be set by
navigating to admin/config/user-interface/jobvite-careers and setting up the url
for the XML file provided by Jobvite.

The markup and CSS for both the listing and individual pages can be edited. To
edit the markup, copy the required tpl.php file to your templates folder and 
edit here.

The careers page will be created in /careers. The individual pages will be 
created at /careers/[jobvite-position-id].


KNOWN ISSUES
------------

There are no known issues at this time.
