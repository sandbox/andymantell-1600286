<?php
/**
 * @file
 * Template file for each individual position.
 *
 * Available variables:
 *
 * $id => The position id from Jobvite
 * $requisitionid => The requisition id from Jobvite
 * $title => The position's title
 * $category => The position's category in Jobvite
 * $jobtype => The job type from Jobvite
 * $date => The date the position was created in Jobvite
 * $detail_url => The full detail url from Jobvite
 * $apply_url => Link to the Jobvite application webform for
 *               the specified position
 * $location => Location where the position is required
 * $description => The job's description
 * $briefdescription => The job's brief description
 *
 * Developed by Marcos Mellado, 2012 - marcos@mellados.com
 */
?>

<div class="jobvite-career" id="jobvite-career-<?php echo $id; ?>">
	<div class="header">
		<h3>Category</h3>
    <p><?php echo $category; ?></p>

    <h3>Location</h3>
    <p><?php echo $location; ?></p>
  </div>

	<div class="description">
    <h3>Description</h3>
		<?php echo $description; ?>
	</div>

  <div class="footer">
    <p><?php echo l(t('Apply Now'), $apply_url, array('attributes' => array('target' => '_blank', 'class' => 'button'))); ?></p>
    <p><?php echo l(t('Back to career listing'), 'careers'); ?></p>
	</div>
</div>
